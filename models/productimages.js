'use strict';
module.exports = function(sequelize, DataTypes) {
  var productImages = sequelize.define('productImages', {
    pcode: DataTypes.STRING,
    url: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here

          //models.productimages.belongsTo(models.product, {as:"product", foreignKey:"productId"});

          models.productImages.belongsTo(models.product, {as:"product", foreignKey:"pcode"});
      }
    }
  });
  return productImages;
};