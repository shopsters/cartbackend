'use strict';
module.exports = function(sequelize, DataTypes) {
  var Payment = sequelize.define('Payment', {
    COD: DataTypes.INTEGER,
    Card: DataTypes.INTEGER,
    Shipping: DataTypes.INTEGER,
    Checkout: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          models.Payment.hasOne(models.Order, {as:"Order", foreignKey:"OrderId"});

      }
    }
  });
  return Payment;
};