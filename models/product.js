'use strict';
module.exports = function(sequelize, DataTypes) {
  var product = sequelize.define('product', {
    pcode: DataTypes.STRING,
    price: DataTypes.STRING,
    specification: DataTypes.STRING,
    Quantity: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          models.product.belongsToMany(models.cart, {as:"carts",through:models.cart2product});
        //  models.product.hasMany(models.images, {as:"Images", foreignKey:"productId"});

          models.product.hasMany(models.productImages, {as:"Images", foreignKey:"pcode"});

      }
    }
  });
  return product;
};