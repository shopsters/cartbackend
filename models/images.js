'use strict';
module.exports = function(sequelize, DataTypes) {
  var images = sequelize.define('images', {
    url: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          models.images.belongsTo(models.product, {as:"product", foreignKey:"productId"});
      }
    }
  });
  return images;
};