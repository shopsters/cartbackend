'use strict';
module.exports = function(sequelize, DataTypes) {
  var customer = sequelize.define('customer', {
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    Gender: DataTypes.STRING,
    Name: DataTypes.STRING,
    Address: DataTypes.STRING,
    phno: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
          models.customer.hasOne(models.cart, {as:"cart", foreignKey:"customerId"});
      }
    }
  });
  return customer;
};