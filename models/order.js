'use strict';
module.exports = function(sequelize, DataTypes) {
  var Order = sequelize.define('Order', {
    OrderStatus: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          models.Order.belongsTo(models.cart, {as:"cart", foreignKey:"CartId"});
          models.Order.belongsTo(models.Payment, {as:"Payment", foreignKey:"PaymentId"});

      }
    }
  });
  return Order;
};