'use strict';
module.exports = function(sequelize, DataTypes) {
  var cart = sequelize.define('cart', {
    costperItem: DataTypes.STRING,
    price: DataTypes.STRING,
    specification: DataTypes.STRING,
    Totalcost: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
          models.cart.belongsToMany(models.product, {as:"products",through:models.cart2product});
          models.cart.belongsTo(models.customer, {as:"customer", foreignKey:"customerId"});

      }
    }
  });
  return cart;
};