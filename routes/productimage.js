/**
 * Created by HP on 7/1/2017.
 */

var express = require('express');
var router = express.Router();
var model = require('../models');

/* GET users listing. */
router.get('/', function(req, res, next) {
    //res.send('respond with a resource');
    model.productImages.findAll().then(function (image) {
        res.json(image);
    });
});

//another get for retrieving specific image
router.get('/:pcode', function(req, res, next)
{
    //res.send('respond with a resource');
    var param = req.params;
    console.log(param.pcode);
    model.productImages.find({
        where:{
            pcode:param.pcode
        }
    }).then(function (image) {
        res.json(image); //.url
    });
});

router.post('/', function(req, res, next) {

    var post = req.body;
    // console.log(req.body.username);


    model.productImages.create({
        url:post.url,
        pcode:post.pcode
    });

});

module.exports = router;