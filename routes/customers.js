var express = require('express');
var router = express.Router();
var model = require('../models');

router.param('id',function (req, res, next, id) {
    console.log('Called only once');
    next();
});
/* GET users listing. */
router.get('/', function(req, res, next) {
    //res.send('respond with a resource');
    model.customer.findAll().then(function (customers) {
        res.json(customers);
    });
});

router.get('/:id',function (req, res, next) {
    var param = req.params;

    model.customer.find({
        where:{
            id:param.id
        }
    })
        .then(function (customers) {
            res.json(customers);

        });
});

router.post('/login', function(req, res, next) {

    var post = req.body;
    model.customer.find({where:{email:post.email}}).then(function (customer) {
        if(customer.password==post.password) {
            res.status= 200;
            res.json(customer.token);
        }
        else{
            res.sendStatus(401)
        }
    });
});



router.post('/', function(req, res, next) {

    var post = req.body;
    console.log(req.body.username);


    model.customer.create({
        email:post.email,
        password:post.password,
        Gender:post.Gender,
        Name:post.Name,
        Address:post.Address,
        phno:post.phno
    });

});

module.exports = router;
/**
 * Created by admin on 4/26/2017.
 */
